


MINH TOKEN 
=======================


*Ho Chi Minh Token is made to replace the QT which trades on Yobit Exchange: https://yobit.net/en/trade/MINH/BTC The MINH wallet at Yobit is not accepting deposits, so this will replace it. Coin count on 18th Jan 2018 is 6214621.77969586 at block 45616. I am issuing 6,214,621 tokens to swap for coins. In future, the Waves client can be used instead of the MINH QT.*



-----


![logo](http://explorer.vietminh.info:3001/images/logo.png)       ![waves](https://cdn.pbrd.co/images/H3zd8X4.png)


-----


**ASSET DETAILS**

 FIELD       |  DESCRIPTION     |     
 --------  |  --------   
Issuer:         |   [3P3qrPeeFxg83Vm3xQ4YP1PupoUUAMBXtDA](https://wavesexplorer.com/address/3P3qrPeeFxg83Vm3xQ4YP1PupoUUAMBXtDA)   |3P3qrPeeFxg83Vm3xQ4YP1PupoUUAMBXtDA(https://wavesexplorer.com/tx/2RrpNmBbvzSZrb8Qp2frQEbQJvX4DNpnoEor78RWK7EP)  | 
Identifier:	      |  [2RrpNmBbvzSZrb8Qp2frQEbQJvX4DNpnoEor78RWK7EP](https://wavesexplorer.com/tx/2RrpNmBbvzSZrb8Qp2frQEbQJvX4DNpnoEor78RWK7EP)    | 
Name       |  MINH (HoChiMinh)    |
Details       |  Ho Chi Minh Token is made to replace the QT which trades on Yobit Exchange: [https://yobit.net/en/trade/MINH/BTC](https://yobit.net/en/trade/MINH/BTC) The MINH wallet at Yobit is not accepting deposits, so this will replace it. Coin count on 18th Jan 2018 is 6214621.77969586 at block 45616. I am issuing 6,214,621 tokens to swap for coins. In future, the Waves client can be used instead of the MINH QT. The Bitbucket repo for MINH is: [https://bitbucket.org/yalta1945/minh/overview](https://bitbucket.org/yalta1945/minh/overview) I have added a Bitbucket repo for the token at the same address: [https://bitbucket.org/yalta1945/minhtoken](https://bitbucket.org/yalta1945/minhtoken)  | 
Total tokens:       |  6214621.00000000    | 
Reissuable:         | No | 
Issue date:         | 1/19/2018 0:06:21 | 



>- [support.wavesplatform.com](support.wavesplatform.com)




-----


LINKS 
=======================

[Waves Decentralized Exchange](https://beta.wavesplatform.com/)

[Waves Platform](https://wavesplatform.com/)

[Yobit Exchange](https://yobit.net/en/trade/MINH/BTC) 

[Waves Explorer](https://wavesexplorer.com/tx/2RrpNmBbvzSZrb8Qp2frQEbQJvX4DNpnoEor78RWK7EP)

[(HoChi) MINH Repository](https://bitbucket.org/yalta1945/minh/overview)



-----


Minh Explorer
=============



![Explorer](https://cdn.pbrd.co/images/H3zePlXe.png)


-----




